
describe('adder tests', function()
{
	it('should pass this canary test', function()
	{
		expect(true).to.eql(true);
	});
	
	it('1 + 2 should equal 3', function()
	{
		expect(add(1,2)).to.eql(3);
	});
});